FROM rasa/rasa:2.6.2-full

USER root

ENV BOT_ENV=production

COPY . /var/www
WORKDIR /var/www

RUN python3 -m venv env
RUN source ./env/bin/activate 
RUN pip install --upgrade pip 
RUN pip install rasa==2.6.2
RUN rasa train

ENTRYPOINT [ "rasa", "run", "-p", "8080" ]